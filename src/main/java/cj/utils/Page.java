package cj.utils;


import org.apache.commons.collections.IteratorUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Page<T> implements Serializable {

    protected int pageNo = 1;
    protected int pageSize = 20; // 默认为每页20条记录
    protected List<T> result = Collections.emptyList(); // 用于封装结果集
    protected long totalCount = 0; // 总记录数

    // -- 构造函数 --//
    public Page() {
    }

    public Page(int pageSize) {
        setPageSize(pageSize);
    }

    public Page(int pageNo, int pageSize) {
        setPageNo(pageNo);
        setPageSize(pageSize);
    }

    public Page(int pageNo, int pageSize, List<T> result, long totalCount) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.result = result;
        this.totalCount = totalCount;
    }

    /**
     * 获得当前页的页号,序号从1开始,默认为1.
     */
    public int getPageNo() {
        return pageNo;
    }

    /**
     * 设置当前页的页号,序号从1开始,低于1时自动调整为1.
     */
    public void setPageNo(final int pageNo) {
        this.pageNo = pageNo;
        if (pageNo < 1) {
            this.pageNo = 1;
        }
    }

    /**
     * 获得每页的记录数量,默认为20
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * 设置每页的记录数量,低于1时自动调整为1.
     */
    public void setPageSize(final int pageSize) {
        this.pageSize = pageSize;
        if (pageSize < 1) {
            this.pageSize = 1;
        }
    }

    /**
     * 根据pageNo和pageSize计算当前页第一条记录在总结果集中的位置,序号从1开始
     */
    public int getFirst() {
        return ((pageNo - 1) * pageSize) + 1;
    }

    /**
     * 根据pageNo和pageSize计算当前页第一条记录在总结果集中的位置,序号从0开始 用于Mysql
     */
    public int getOffset() {
        return ((pageNo - 1) * pageSize);
    }

    /**
     * 实现Iterable接口,可以for(Object item : page)遍历使用
     */
    @SuppressWarnings("unchecked")
    public Iterator<T> iterator() {
        return result == null ? IteratorUtils.EMPTY_ITERATOR : result
                .iterator();
    }

    /**
     * 取得页内的记录列表.
     */
    public List<T> getResult() {
        return result;
    }

    /**
     * 设置页内的记录列表.
     */
    public void setResult(final List<T> result) {
        this.result = result;
    }

    /**
     * 取得总记录数, 默认值为-1.
     */
    public long getTotalCount() {
        return totalCount;
    }

    /**
     * 设置总记录数.
     */
    public void setTotalCount(final long totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * 是否第一页.
     */
    public boolean getFirstPage() {
        return pageNo == 1;
    }

    /**
     * 是否还有上一页
     */
    public boolean hasPrePage() {
        return (pageNo - 1 >= 1);
    }

    /**
     * 是否还有下一页.
     */
    public boolean hasNextPage() {
        return (pageNo + 1 <= getTotalPages());
    }

    /**
     * 是否最后一页.
     */
    public boolean getLastPage() {
        return pageNo == getTotalPages();
    }

    /**
     * 取得下页的页号, 序号从1开始 当前页为尾页时仍返回尾页序号
     */
    public int getNextPage() {
        if (hasNextPage()) {
            return pageNo + 1;
        } else {
            return pageNo;
        }
    }

    /**
     * 取得上页的页号, 序号从1开始 当前页为首页时返回首页序号
     */
    public int getPrePage() {
        if (hasPrePage()) {
            return pageNo - 1;
        } else {
            return pageNo;
        }
    }

    /**
     * 根据pageSize与totalCount计算总页数, 默认值为0
     */
    public int getTotalPages() {
        if (totalCount < 0) {
            return -1;
        }
        int count =(int) Math.ceil(totalCount / pageSize);
        if (totalCount % pageSize > 0) {
            count++;
        }
        return count;
    }

    public List<Integer> getPageNos() {
        return getPageNos(getTotalPages(), this.pageNo);
    }

    public static List<Integer> getPageNos(int totalNo, int pageNo) {
        List<Integer> r = new ArrayList<Integer>();
        //long totalNo=getTotalPages();
        int startNo = 1;
        int endNo = totalNo;
        if (pageNo < 5) {
            startNo = 1;
            endNo = (int) (totalNo > 5 ? 5 : totalNo);
            for (int i = startNo; i <= endNo; i++) {
                r.add(i);
            }
            if (totalNo > 5) {
                r.add(0);
            }
        } else if (pageNo >= totalNo - 3) {
            startNo = pageNo - 3;
            endNo =  totalNo;
            r.add(1);
            if (startNo > 2) {
                r.add(2);
                if (startNo != 3) {
                    r.add(0);
                }

            }
            for (int i = startNo; i <= endNo; i++) {
                r.add(i);
            }
        } else {
            r.add(1);
            startNo = pageNo - 3;
            endNo = pageNo + 3;
            if (startNo > 2) {
                r.add(0);
            }
            for (int i = startNo; i <= endNo; i++) {
                r.add(i);
            }
            if (endNo < totalNo) {
                if (endNo < totalNo - 1) {
                    r.add(0);
                }

                r.add(totalNo);
            }

        }

        return r;
    }


    public static void main(String[] args) {
        List<Integer> r = getPageNos(15, 5);
        for (Integer i : r) {
            System.out.print(i + " ");
        }
    }
}

