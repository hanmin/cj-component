package cj.utils;

public class Constants {

	public static String charset = "utf-8";  
	
	//返回对象
	public interface returnObj
	{
		//0:true
		public String returnFlag_true="0";
		//1:flase
		public String returnFlag_flase="1";
		//2:操作重复
		public String returnFlag_repeat="2";
		//3:操作人自己
		public String returnFlag_3="3";
		
		public interface returnCode
		{
			//A001:已存在
			public String code_1="A001";
			//A002:不存在
			public String code_2="A002";
			//A003:状态不符合
			public String code_3="A003";
			//A004:密码错误
			public String code_4="A004";
			//A005:超过约定数量
			public String code_5="A005";
			//A006:结束
			public String code_6="A006";
			
			public String code_7="A007";
			//A008:未开始
			public String code_8="A008";
		}
		
		public interface returnPraise
		{
			//P001:点赞
			public String code_1="P001";
			//P002:取消点赞
			public String code_2="P002";
		}
	}
	
	public static interface flag
	//flag（1、是；0、否）
	{
		public static int type0=0;
		public static int type1=1;
	}
	
	//门户
	public interface portal
	{
		//发布状态（0.不发布；1.发布；2.撤销）
		public interface releaseState
		{
			//0:不发布
			public int state_0=0;
			//1:发布
			public int state_1=1;
			//2:撤销
			public int state_2=2;
		}
		
		//发布状态（0.不在线；1.在线）
		public interface userState
		{
			//0:不在线
			public int state_0=0;
			//1:在线
			public int state_1=1;
		}
	} 
	
	//删除标记 0:删除 1:没删除
	public interface deleteFlag
	{
		//0:删除
		public int deleteFlag_0=0;
		//1:没删除
		public int deleteFlag_1=1;
	}
	
	/**
	 * 微信Key
	 * @author dell
	 *
	 */
	public static interface weixinkey
	{
		//AppID
		public static String AppID="wxb8851d53461b983d";
		//AppSecret
		public static String AppSecret="99d0b770cd1b60a8103cfc2f62b1bf53";
		//微信初始化请求
		public static String weixinUrl="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
		//获取用户增减数据（getusersummary）
		public static String getusersummary="https://api.weixin.qq.com/datacube/getusersummary?access_token=";
		//获取累计用户数据（getusercumulate）
		public static String getusercumulate="https://api.weixin.qq.com/datacube/getusercumulate?access_token=";
		
		//获取图文群发每日数据（getarticlesummary）
		public static String getarticlesummary="https://api.weixin.qq.com/datacube/getarticlesummary?access_token=";
		//获取图文群发总数据（getarticletotal）
		public static String getarticletotal="https://api.weixin.qq.com/datacube/getarticletotal?access_token=";
		//获取图文统计数据（getuserread）
		public static String getuserread="https://api.weixin.qq.com/datacube/getuserread?access_token=";
		//获取图文统计分时数据（getuserreadhour）
		public static String getuserreadhour="https://api.weixin.qq.com/datacube/getuserreadhour?access_token=";
		//获取图文分享转发数据（getusershare）
		public static String getusershare="https://api.weixin.qq.com/datacube/getusershare?access_token=";
		//获取图文分享转发分时数据（getusersharehour）
		public static String getusersharehour="https://api.weixin.qq.com/datacube/getusersharehour?access_token=";
		
		//获取消息发送概况数据（getupstreammsg）		
		public static String getupstreammsg="https://api.weixin.qq.com/datacube/getupstreammsg?access_token=";
		//获取消息分送分时数据（getupstreammsghour）		
		public static String getupstreammsghour="https://api.weixin.qq.com/datacube/getupstreammsghour?access_token=";
		//获取消息发送周数据（getupstreammsgweek）	
		public static String getupstreammsgweek="https://api.weixin.qq.com/datacube/getupstreammsgweek?access_token=";
		//获取消息发送月数据（getupstreammsgmonth）	
		public static String getupstreammsgmonth="https://api.weixin.qq.com/datacube/getupstreammsgmonth?access_token=";
		//获取消息发送分布数据（getupstreammsgdist）		
		public static String getupstreammsgdist="https://api.weixin.qq.com/datacube/getupstreammsgdist?access_token=";
		//获取消息发送分布周数据（getupstreammsgdistweek）		
		public static String getupstreammsgdistweek="https://api.weixin.qq.com/datacube/getupstreammsgdistweek?access_token=";
		//获取消息发送分布月数据（getupstreammsgdistmonth）		
		public static String getupstreammsgdistmonth="https://api.weixin.qq.com/datacube/getupstreammsgdistmonth?access_token=";
		
		//微信JS接口的临时票据（jsapiticketurl）		
		public static String jsapiticketurl="https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=";
				
		/**
		 * 请求消息类型：事件
		 */
		public static final String REQ_MESSAGE_TYPE_EVENT = "event";

		/**
		 * 事件类型：subscribe(关注)
		 */
		public static final String EVENT_TYPE_SUBSCRIBE = "subscribe";

		/**
		 * 事件类型：unsubscribe(取消关注)
		 */
		public static final String EVENT_TYPE_UNSUBSCRIBE = "unsubscribe";

		/**
		 * 事件类型：CLICK(自定义菜单点击事件)
		 */
		public static final String EVENT_TYPE_CLICK = "CLICK";
		
		/**
		 * 返回消息类型：文本
		 */
		public static final String RESP_MESSAGE_TYPE_TEXT = "text";
		/**
		 * 返回消息类型：视图
		 */
		public static final String RESP_MESSAGE_TYPE_VIEW = "VIEW";
		
		/**
		 * TOKEN
		 */
		public static final String WEIXIN_TOKEN_MARRY = "marry";
	}
	
	/**
	 * 微博Key
	 * @author dell
	 *
	 */
	public static interface weibokey
	{
		public static String access_token = "2.00x4CyIGXgnuCD54d9a461ddbltWjD";
		public static String uids = "5629316533";
		
		public static String client_ID = "2791488529";
		public static String client_SERCRET = "d52477ef4e5dc5c82d32e3a3e6374f2e";
		public static String redirect_URI = "https://www.baidu.com";
		public static String baseURL="https://api.weibo.com/2/";
		public static String accessTokenURL="https://api.weibo.com/oauth2/access_token";
		public static String authorizeURL="https://api.weibo.com/oauth2/authorize";
		public static String rmURL="https://rm.api.weibo.com/2/";
	}
	
	/**
	 * app变量
	 * @author dell
	 *
	 */
	public static interface appkey
	{
		public static interface appVisitType
			//1、下载；2、安装；3、注册；4活跃；5卸载
		{
			public static String type1="1";
			public static String  type2="2";
			public static String  type3="3";
			public static String  type4="4";
			public static String  type5="5";
		}
		
		public static interface accountType
		//用户类型（1、正常；2、匿名）
		{
			public static int type1=1;
			public static int type2=2;
		}
		
		public static interface columnType
		//用户类型（5、订阅；6、取消订阅）
		{
			public static String type5="5";//订阅
			public static String type6="6";//取消订阅
		}
		
		public static String columnPids="1";
		
		public static String sysid_changjiangxinwen="1";
	}
	
	public static interface signkey
	{
		//最武汉key
		public static final String signkey_zwh = "82a54e889c004e3fadda170cf1bf8290";
	}
	
	/**
	 * 集团产业
	 * @author Administrator
	 *
	 */
	public static interface orgKey
	{
		//快递
		public static interface kdKey
		{
			//快递公司首页导航滚动
			public static String SYDHGD="SYDHGD";
			//关于我们
			public static String GYWM="GYWM";
			//联系我们
			public static String LXWM="LXWM";
			//招贤纳士
			public static String ZXNS="ZXNS";
			//公司业务-广告分公司
			public static String GGFGS="GGFGS";
			//公司业务-速递分公司
			public static String SDFGS="SDFGS";
			//公司业务-商务分公司
			public static String SWFGS="SWFGS";
			//公司业务-物流分公司
			public static String WLFGS="WLFGS";
		}
	}
	
	//管理员账户
	public static final String ADMIN = "admin"; 
	
	/**
	 * 活动组常量
	 * @author dell
	 *
	 */
	public interface Activity
	{
		//现场下载推广
		public final String ActivityId_5="5";
		//汉街摇奖
		public final String ActivityId_6="6";
		
		//0、待确认;1、已确认;2、无效;3、未中奖
		public final int Status_0 =0;
		public final int Status_1 =1;
		public final int Status_2 =2;
		public final String Status_3 ="-1";
		
		//活动为开始	A00:开始		A01:未开始	A02:你已抽过奖
		public final String code_A00="A00";
		public final String code_A01="A01";
		public final String code_A02="A02";
		
		public final String code_A03="A03";//设备已参与报名
		public final String code_A04="A04";//手机已参与报名
		public final String code_A99="A99";//报名成功
	}
}
