package cj.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.AppMessage;
import com.gexin.rp.sdk.base.payload.APNPayload;
import com.gexin.rp.sdk.base.uitls.AppConditions;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.TransmissionTemplate;

public class GeTui {
	//首先定义一些常量, 修改成开发者平台获得的值
    private static String appId = "0NySE0Gv1y7pGOhtChs8VA";
    private static String appKey = "gqKKrl9JfAAkZKEXBz1RfA";
    private static String masterSecret = "7uNp3TwQDp5lU2azCgEHi7";
    private static String url = "http://sdk.open.api.igexin.com/apiex.htm";
    
    /***
     * 推送所有用户文字消息
     * @param title 文字消息标题
     * @param text 文字消息内容
     * @param content json格式内容
     * @param appId 个推提供的appId
     * @param appKey 个推提供的appKey
     * @param masterSecret 个推提供的masterSecret
     * @param isOffline 是否离线推送
     * @param expireTime 离线推送超时时间，单位：毫秒，参数isOffline为false时，该参数无效
     * @throws IOException
     */
    public static void pushToAll(String title, String text, JSONObject content,String appId, String appKey, String masterSecret, boolean isOffline, long expireTime) throws IOException{
        // 新建一个IGtPush实例，传入调用接口网址，appkey和masterSecret
        IGtPush push = new IGtPush(url, appKey, masterSecret);
        
        // 新建一个推送模版, 以链接模板为例子，就是说在通知栏显示一条含图标、标题等的通知，用户点击可打开您指定的网页
        NotificationTemplate template = new NotificationTemplate();
        // 设置APPID与APPKEY
        template.setAppId(appId);
        template.setAppkey(appKey);
        // 设置通知栏标题与内容
//        if(title.length() >= 20){
//        	title = title.substring(0,17) + "...";
//        }
        if(text.length() >= 50){
        	text = text.substring(0,47) + "...";
        }
        template.setTitle(title);
        template.setText(text);
        // 设置通知是否响铃，震动，或者可清除
        template.setIsRing(true);
        template.setIsVibrate(true);
        template.setIsClearable(true);
        // 透传消息设置，1为强制启动应用，客户端接收到消息后就会立即启动应用；2为等待应用启动
        template.setTransmissionType(1);
        template.setTransmissionContent(content.toString());
        
        List<String> appIds = new ArrayList<String>();
        appIds.add(appId);

        AppMessage message = new AppMessage();
        message.setData(template);
        message.setAppIdList(appIds);
        message.setOffline(isOffline);
        if(isOffline){
        	message.setOfflineExpireTime(expireTime);
        }
        
        //只对ANDROID发送通知推送
        List<String> phoneTypeList = new ArrayList<String>();
        phoneTypeList.add("ANDROID");
        AppConditions cdt = new AppConditions();
        cdt.addCondition(AppConditions.PHONE_TYPE, phoneTypeList);
        message.setConditions(cdt);

        IPushResult ret = push.pushMessageToApp(message);
    }
    
    public static void pushToAll(String title, String text, JSONObject content) throws IOException{
    	pushToAll(title, text, content,appId, appKey, masterSecret, true, 1000*600);
    }
    
    public static void transPushToAll(String title, Map content, boolean isOffline, long expireTime) throws IOException{
        IGtPush push = new IGtPush(url, appKey, masterSecret);

        TransmissionTemplate template = getTemplate(title, content);
        
        List<String> appIds = new ArrayList<String>();
        appIds.add(appId);

        AppMessage message = new AppMessage();
        message.setData(template);
        message.setAppIdList(appIds);
        message.setOffline(isOffline);
        if(isOffline){
        	message.setOfflineExpireTime(expireTime);
        }
        
        //只对IOS发送透传推送
        List<String> phoneTypeList = new ArrayList<String>();
        phoneTypeList.add("IOS");
        AppConditions cdt = new AppConditions();
        cdt.addCondition(AppConditions.PHONE_TYPE, phoneTypeList);
        message.setConditions(cdt);

        IPushResult ret = push.pushMessageToApp(message);
    }
    
    public static TransmissionTemplate getTemplate(String title, Map content) {
        TransmissionTemplate template = new TransmissionTemplate();
        template.setAppId(appId);
        template.setAppkey(appKey);
        template.setTransmissionType(1);
        template.setTransmissionContent(content.toString());
        APNPayload payload = new APNPayload();
        for(Object key : content.keySet()){
        	payload.addCustomMsg(key.toString(), content.get(key));
        }
        payload.setBadge(1);
        payload.setContentAvailable(1);
        payload.setSound("default");
        payload.setCategory("$由客户端定义");
        //简单模式APNPayload.SimpleMsg 
        payload.setAlertMsg(new APNPayload.SimpleAlertMsg(title));
        
        //字典模式使用下者
        //payload.setAlertMsg(getDictionaryAlertMsg());
        template.setAPNInfo(payload);
        return template;
    }
    
    private static APNPayload.DictionaryAlertMsg getDictionaryAlertMsg(){
        APNPayload.DictionaryAlertMsg alertMsg = new APNPayload.DictionaryAlertMsg();
        alertMsg.setBody("body");
        alertMsg.setActionLocKey("ActionLockey");
        alertMsg.setLocKey("LocKey");
        alertMsg.addLocArg("loc-args");
        alertMsg.setLaunchImage("launch-image");

        alertMsg.setTitle("Title");
        alertMsg.setTitleLocKey("TitleLocKey");
        alertMsg.addTitleLocArg("TitleLocArg");
        return alertMsg;
    }
    
    public static void main(String[] args) throws IOException {
    }

}
