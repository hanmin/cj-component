package cj.utils;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class ReturnUUID {

	public static String getUUID()
	{
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	/**
	 * 依据原始文件名生成新文件名
	 * @return
	 */
	public static String getName(String fileName) {
		Random random = new Random();
		return fileName = "" + random.nextInt(10000)
				+ System.currentTimeMillis() + getFileExt(fileName);
	}
	
	/**
	 * 依据原始文件名生成新文件名
	 * @return
	 */
	public static String getRandomNum() {
		int max=10000;
        int min=1000;
        Random random = new Random();
        int num = random.nextInt(max)%(max-min+1) + min;
		return String.valueOf(num);
	}
	
	/**
	 * 依据原始文件名生成新文件名	10000-1000
	 * @param max	10000
	 * @param min	1000
	 * @return
	 */
	public static String getRandomNum(int max,int min) {
        Random random = new Random();
        int num = random.nextInt(max)%(max-min+1) + min;
		return String.valueOf(num);
	}
	
	/** 
     * 生成订单编号 
     * @return 
     */  
    public static synchronized String getOrderNo(String head) {  
        
        String str = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date());  
        return head+str;  
    }  
    
	/**
	 * 获取文件扩展名
	 * 
	 * @return string
	 */
	public static String getFileExt(String fileName) {
		return fileName.substring(fileName.lastIndexOf("."));
	}
	
	/**
	 * 根据字符串创建本地目录 并按照日期建立子目录返回
	 * @param path 
	 * @return 
	 * filepath	上传后下一级文件名
	 * allpath	全路径
	 */
	public static Map getFolder(String path) {
		Map rep = new HashMap();
		//SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd");
		//String filepath = formater.format(new Date());
		
		String filepath = String.valueOf(System.currentTimeMillis());
		
		path += "/" + filepath.substring(filepath.length()-3, filepath.length());
		File dir = new File(path);
		if (!dir.exists()) {
			try {
				dir.mkdirs();
			} catch (Exception e) {
				return rep;
			}
		}
		rep.put("filepath", filepath.substring(filepath.length()-3, filepath.length()));
		rep.put("allpath", path);
		return rep;
	}
	
	/**
	 * 根据传入进来的类型，格式化数字
	 * @param pattern	格式化的数字
	 * @param num		数字
	 * @return
	 */
	public static String NumFormat(String pattern,int num){
		return new DecimalFormat("000000").format(num);
	}
	
	public static void main(String[] args)
	{
//		String str="jsgl@hjzx@pz@yhgl@cdgl@jtgw@fbnr@fbjj@pzgl@btdt@yhlb@xmtjz@blxx@xrpm@fxwd@";
//		
//		System.out.println(System.currentTimeMillis());
//		String filepath = String.valueOf(System.currentTimeMillis());
//		System.out.println(filepath.substring(filepath.length()-3, filepath.length()));
//		
//		String strfile = "652//83271452155442653.png";
//		
//		System.out.println("s_"+strfile.split("//")[1]);
		
//		System.out.println(getOrderNo("OS"));
//		System.out.println(getRandomNum());
//		System.out.println(getUUID());
		System.out.println(NumFormat("000000", 115));
	}
}
