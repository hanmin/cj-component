package cj.utils.weixin;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import cj.utils.Constants;

/**
 * 微信签名
 * @author Administrator
 *
 */
public class CheckUtil {
	
	
	
	/**
	 * 验证签名
	 * 
	 * @param request
	 * @return
	 */
	public static boolean checkSignature(HttpServletRequest request) {
		
		/*
		 * signature 微信加密签名，
		 * signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
		 * timestamp 时间戳 
		 * nonce 随机数
		 */
		String signature = request.getParameter("signature");
		String timestamp = request.getParameter("timestamp");
		String nonce = request.getParameter("nonce");
		
		if (nonce != null && signature != null && timestamp != null) {
			String[] arr = new String[] { Constants.weixinkey.WEIXIN_TOKEN_MARRY, timestamp, nonce };
			// 将token、timestamp、nonce三个参数进行字典排序
			Arrays.sort(arr);
			StringBuilder content = new StringBuilder();
			for (int i = 0; i < arr.length; i++) {
				content.append(arr[i]);
			}
			MessageDigest md = null;
			String tmpStr = null;
			
			try {
				md = MessageDigest.getInstance("SHA-1");
				// 将三个参数字符串拼接成一个字符串进行sha1加密
				byte[] digest = md.digest(content.toString().getBytes());
				tmpStr = byteToStr(digest);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

			content = null;
			// 将sha1加密后的字符串可与signature对比
			return tmpStr != null ? tmpStr.equals(signature.toUpperCase()) : false;
		} else {
			return false;
		}
	}

	/**
	 * 将字节数组转换为十六进制字符串
	 * 
	 * @param byteArray
	 * @return
	 */
	private static String byteToStr(byte[] byteArray) {
		String strDigest = "";
		for (int i = 0; i < byteArray.length; i++) {
			strDigest += byteToHexStr(byteArray[i]);
		}
		return strDigest;
	}

	/**
	 * 将字节转换为十六进制字符串
	 * 
	 * @param mByte
	 * @return
	 */
	private static String byteToHexStr(byte mByte) {
		char[] Digit = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
				'B', 'C', 'D', 'E', 'F' };
		char[] tempArr = new char[2];
		tempArr[0] = Digit[(mByte >>> 4) & 0X0F];
		tempArr[1] = Digit[mByte & 0X0F];

		String s = new String(tempArr);
		return s;
	}

}
