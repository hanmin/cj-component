package cj.utils.weixin.pay;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import cj.utils.MD5Security;
import cj.utils.ReturnUUID;

public class OrderQuery extends PayConstants{

	/**
	 * 返回订单交易状态
	 * @param transaction_id
	 * @return
	 * @throws Exception
	 */
	public String getOrderPayStatus(String transaction_id) throws Exception
	{
		//随机参数
		String nonce_str = ReturnUUID.getUUID();
		String sign = returnSignMd5(APPID, MCH_ID, nonce_str, transaction_id);
		
		StringBuffer requestContent=returnRequestContent(nonce_str, sign, transaction_id);
		
		String return_code="";
		
		//请求远端微信Http接口
		try {
			byte[] xml = HttpsUtil.post(ORDERQUERY, requestContent.toString(), CHARSETUTF8);
			return_code=returnCodeForXml(new String(xml, CHARSETUTF8));
		} catch (KeyManagementException | NoSuchAlgorithmException
				| IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return return_code;
	}
	
	/**
	 * 
	 * @param appid
	 * @param mch_id
	 * @param nonce_str
	 * @param transaction_id
	 * @param out_trade_no
	 * @return
	 * @throws Exception
	 */
	private String returnSignMd5(String appid,String mch_id,String nonce_str,String transaction_id) throws Exception
	{
		//step1-----拼接字符串，key value 形式
		appid="appid="+appid;
		mch_id="mch_id="+mch_id;
		nonce_str="nonce_str="+nonce_str;
		transaction_id="transaction_id="+transaction_id;
		
		String[] arr = new String[] {appid, mch_id, nonce_str, transaction_id};
		//step2-----将参数进行字典排序
		Arrays.sort(arr);
		StringBuilder content = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			content.append(arr[i]+"&");
		}
		//step3-----将 key=xxx 加入最末位
		String ss = content+"key="+KEY;
		//System.out.println(ss);
		//step4-----将拼接号的字符串，MD5加密32位
		String sign_md5 = MD5Security.md5(ss);
		//System.out.println("sign----"+sign_md5);
		return sign_md5.toUpperCase();
	}
	
	/**
	 * 
	 * @param nonce_str
	 * @param sign
	 * @param transaction_id
	 * @return
	 * @throws Exception
	 */
	private StringBuffer returnRequestContent(String nonce_str,String sign,String transaction_id) throws Exception
	{
		//构成统一下单接口XML结构内容
		StringBuffer requestContent= new StringBuffer();
		requestContent.append("<xml>");
		requestContent.append("<appid><![CDATA["+APPID+"]]></appid>");
		requestContent.append("<mch_id><![CDATA["+MCH_ID+"]]></mch_id>");
		requestContent.append("<nonce_str><![CDATA["+nonce_str+"]]></nonce_str>");
		requestContent.append("<transaction_id><![CDATA["+transaction_id+"]]></transaction_id>");
		requestContent.append("<sign><![CDATA["+sign+"]]></sign>");
		requestContent.append("</xml>");
		return requestContent;
	}
	
	private String returnCodeForXml(String requestContent)
	{
		Map rep = new HashMap();
		Document doc = Jsoup.parse(requestContent);
		//请求返回编码
		return doc.getElementsByTag("return_code").first().text();
	}
}
