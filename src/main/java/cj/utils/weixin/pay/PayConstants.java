package cj.utils.weixin.pay;

/**
 * 微信支付常量
 * @author Administrator
 *
 */
public class PayConstants {
	/**婚典**/
	//公众账号ID
	public final static String APPID ="wxb8851d53461b983d";
	//商户号
	public final static String MCH_ID ="1352398401";
	//交易类型
	public final static String TRADE_TYPE ="JSAPI";
	//货币类型
	public final static String FEE_TYPE ="CNY";
	//统一下单链接地址
	public final static String UNIFIEDORDERURL ="https://api.mch.weixin.qq.com/pay/unifiedorder";
	
	public final static String ORDERQUERY ="https://api.mch.weixin.qq.com/pay/orderquery";
	//字符集utf-8
	public final static String CHARSETUTF8 ="utf-8";
	//婚典商户key设置
	public final static String KEY ="c3910797bffb448aa612f188d56fd388";
	
	//请求返回SUCCESS
	public final static String SUCCESS ="SUCCESS";
	//请求返回FAIL
	public final static String FAIL ="FAIL";
	//signType
	public final static String SIGNTYPE ="MD5";
}
