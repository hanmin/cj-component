package cj.utils.weixin.pay;

import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.http.protocol.HTTP;

import cj.utils.Constants;
import cj.utils.HttpClientUtil;
import cj.utils.weixin.HttpsWenXinUtil;

public class WXConfigUtil {
	
	public static void main(String[] args) throws Exception
	{
		String appid = Constants.weixinkey.AppID;
		String secret = Constants.weixinkey.AppSecret;
		String jsapi_ticket = returnJsapiTicket(appid,secret);
		System.out.println("jsapi_ticket---"+jsapi_ticket);
		String requestUrl = "http://marriage.cjrbjt.com/order/orderConfirm?goodsItemIds=67&counts=1";
		
		Map<String, String> ret = Sign.sign(jsapi_ticket, requestUrl);
        for (Map.Entry entry : ret.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
	}

	/**
	 * 获取JsapiTicket
	 * @param appid
	 * @param secret
	 * @param requestUrl
	 * @param noncestr
	 * @param timestamp
	 * @return
	 * @throws Exception
	 */
	public static String returnJsapiTicket(String appid,String secret) throws Exception
	{
		String access_token = HttpsWenXinUtil.initWeiXin(appid,secret);
		//请求远端微信Http接口
		String xml = HttpClientUtil.get(Constants.weixinkey.jsapiticketurl+access_token, HTTP.UTF_8);
		JSONObject jsonObject = JSONObject.fromObject(xml); 
		String ticket = jsonObject.get("ticket").toString();
		return ticket;
	}
}
