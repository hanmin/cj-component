package cj.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
/**
 * 
 * @author dell
 *
 */
public class DateUtils {

	/** 定义常量 **/
	public static final String DATE_JFP_STR = "yyyyMM";
	public static final String DATE_FULL_STR = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_SMALL_STR = "yyyy-MM-dd";
	public static final String DATE_KEY_STR = "yyMMddHHmmss";
	public static final String DATE_SMALL_STR1 = "yyyy/MM/dd";
	public static final String DATE_SMALL_STR2 = "yyyyMMdd";
	public static final String DATE_FULL_STR1 = "yyyyMMddHHmmss";
	
	public static final String DATE_FULL_STR_00 = "yyyy-MM-dd 00:00:00";
	/**
	 * 使用预设格式提取字符串日期
	 * 
	 * @param strDate
	 *            日期字符串
	 * @return
	 */
	public static Date parse(String strDate) {
		return parse(strDate, DATE_FULL_STR);
	}

	/**
	 * 使用用户格式提取字符串日期
	 * 
	 * @param strDate
	 *            日期字符串
	 * @param pattern
	 *            日期格式
	 * @return
	 */
	public static Date parse(String strDate, String pattern) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		try {
			return df.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/***
	 * 格式化string类型的时间，原时间格式、转换的时间格式相同， 主要是为了规范化一下时间格式， 
	 * 例如从数据库中取时间 1970-01-01 00：00：00.0， 调用该方法可以去掉最后的 .0
	 * @param strDate string类型的时间
	 * @param pattern 原时间格式 转换的时间格式 相同
	 * @return
	 * @throws ParseException
	 */
	public static String formatDateStr(String strDate, String pattern) throws ParseException{
		return formatDateStr(strDate,pattern,pattern);
	}
	
	/***
	 * 格式化string类型的时间
	 * @param strDate string类型的时间
	 * @param OrgPattern 原时间格式
	 * @param tagPattern 转换的时间格式
	 * @return 
	 * @throws ParseException
	 */
	public static String formatDateStr(String strDate,  String OrgPattern, String tagPattern) throws ParseException{
		Date date = new SimpleDateFormat(OrgPattern).parse(strDate); 
		String formatDate = new SimpleDateFormat(tagPattern).format(date);
		
		return formatDate;
	}

	/**
	 * 两个时间比较
	 * 
	 * @param date
	 * @return
	 */
	public static int compareDateWithNow(Date date1) {
		Date date2 = new Date();
		int rnum = date1.compareTo(date2);
		return rnum;
	}

	/**
	 * 两个时间比较(时间戳比较)
	 * 
	 * @param date
	 * @return
	 */
	public static int compareDateWithNow(long date1) {
		long date2 = dateToUnixTimestamp();
		if (date1 > date2) {
			return 1;
		} else if (date1 < date2) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * 获取系统当前时间
	 * 
	 * @return
	 */
	public static String getNowTime() {
		SimpleDateFormat df = new SimpleDateFormat(DATE_FULL_STR);
		return df.format(new Date());
	}

	/**
	 * 获取系统当前时间
	 * 
	 * @return
	 */
	public static String getNowTime(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		return df.format(new Date());
	}
	
	/**
	 * 获取系统当前时间
	 * 
	 * @return
	 */
	public static String getDateTime2Str(Date dt,String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		return df.format(dt);
	}
	
	/**
	 * 时间减
	 * @param strdate	传入时间字串
	 * @param type		格式化类型
	 * @param num		减去时间数量
	 * @return
	 */
	public static String getMinusTime(String strdate,String type,int num) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		Date beginDate = parse(strdate, type);
		Calendar date = Calendar.getInstance();
		date.setTime(beginDate);
		date.set(Calendar.DATE, date.get(Calendar.DATE) - num);
		return df.format(date.getTime());
	}
	
	/**
	 * 时间月减
	 * @param strdate	传入时间字串
	 * @param type		格式化类型
	 * @param num		减去时间数量
	 * @return
	 */
	public static String getMinusMonth(String strdate,String type,int month) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		Date beginDate = parse(strdate, type);
		Calendar date = Calendar.getInstance();
		date.setTime(beginDate);
		date.set(Calendar.MONTH, date.get(Calendar.MONTH) - month);
		return df.format(date.getTime());
	}
	
	/**
	 * 时间减
	 * 
	 * @return
	 */
	public static String getMinusTime(String type,int num) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		Date beginDate = new Date();
		Calendar date = Calendar.getInstance();
		date.setTime(beginDate);
		date.set(Calendar.DATE, date.get(Calendar.DATE) - num);
		return df.format(date.getTime());
	}
	
	/**
	 * 时间减
	 * 
	 * @return
	 */
	public static String getAddMinusTime(String strdate,String type,int num) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		Date beginDate = parse(strdate, type);
		Calendar date = Calendar.getInstance();
		date.setTime(beginDate);
		date.set(Calendar.MINUTE, date.get(Calendar.MINUTE) + num);
		return df.format(date.getTime());
	}
	
	/**
	 * 获取系统前天时间
	 * 
	 * @return
	 */
	public static String getYesterdayTime(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);
		Date beginDate = new Date();
		Calendar date = Calendar.getInstance();
		date.setTime(beginDate);
		date.set(Calendar.DATE, date.get(Calendar.DATE) - 1);
		return df.format(date.getTime());
	}

	/**
	 * 获取系统当前计费期
	 * 
	 * @return
	 */
	public static String getJFPTime() {
		SimpleDateFormat df = new SimpleDateFormat(DATE_JFP_STR);
		return df.format(new Date());
	}

	/**
	 * 将指定的日期转换成Unix时间戳
	 * 
	 * @param String
	 *            date 需要转换的日期 yyyy-MM-dd HH:mm:ss
	 * @return long 时间戳
	 */
	public static long dateToUnixTimestamp(String date) {
		long timestamp = 0;
		try {
			timestamp = new SimpleDateFormat(DATE_FULL_STR).parse(date)
					.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return timestamp;
	}

	/**
	 * 将指定的日期转换成Unix时间戳
	 * 
	 * @param String
	 *            date 需要转换的日期 yyyy-MM-dd
	 * @return long 时间戳
	 */
	public static long dateToUnixTimestamp(String date, String dateFormat) {
		long timestamp = 0;
		try {
			timestamp = new SimpleDateFormat(dateFormat).parse(date).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return timestamp;
	}

	/**
	 * 将当前日期转换成Unix时间戳
	 * 
	 * @return long 时间戳
	 */
	public static long dateToUnixTimestamp() {
		long timestamp = new Date().getTime();
		return timestamp;
	}

	/**
	 * 将Unix时间戳转换成日期
	 * 
	 * @param long timestamp 时间戳
	 * @return String 日期字符串
	 */
	public static String unixTimestampToDate(long timestamp) {
		SimpleDateFormat sd = new SimpleDateFormat(DATE_FULL_STR);
		sd.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		return sd.format(new Date(timestamp));
	}
	
	/**
	 * 取相隔时间毫秒数
	 * @param dateA
	 * @param dateB
	 * @return
	 */
	public static long getBetweenMinNumber(String dateA, String dateB) {
		long dayNumber = 0;
		long DAY = 60L * 1000L;
		SimpleDateFormat df = new SimpleDateFormat(DATE_FULL_STR);
		try {
			java.util.Date d1 = df.parse(dateA);
			java.util.Date d2 = df.parse(dateB);
			dayNumber = (d2.getTime() - d1.getTime())/DAY*60L*1000L;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dayNumber;
	}
	
	/* 
     * 将时间戳转换为时间
     */
	public static String stampToDate(String s){
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt*1000);
        res = simpleDateFormat.format(date);
        return res;
    }
	
	/* 
     * 将时间转换为时间戳
     */    
    public static String dateToStamp(String s) throws ParseException{
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = simpleDateFormat.parse(s);
        long ts = date.getTime();
        res = String.valueOf(ts/1000);
        return res;
    }
	
	public static void main(String[] args) throws ParseException {
		
		System.out.println(stampToDate("1478867474"));
		System.out.println(dateToStamp("2016-11-11 20:31:14"));
		
	}
}
