package cj.utils.sms;

import java.util.HashMap;
import java.util.Set;

import cj.utils.Constants;

//import com.cloopen.rest.sdk.CCPRestSDK;

public class SendSMSUtil {
	
	/**
	 * 发送短信
	 * @param phone		手机号
	 * @param tempid	模版ID
	 * @param token		验证码
	 * @param params2	填写“1”
	 * @return
	 */
	public static String sendSMS(String accountSid, String accountToken,String appId,String phone,String tempid,String[] msg)
	{
		String returnFlag = "";
//		HashMap<String, Object> result = null;
//
//		CCPRestSDK restAPI = new CCPRestSDK();
//		restAPI.init("app.cloopen.com", "8883");// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
//		
//		//accountSid, accountToken
//		restAPI.setAccount(accountSid, accountToken);// 初始化主帐号和主帐号TOKEN
//		//AppId
//		restAPI.setAppId(appId);// 初始化应用ID
//		
//		result = restAPI.sendTemplateSMS(phone,tempid,msg);
//
//		System.out.println("SDKTestSendTemplateSMS result=" + result);
//		if("000000".equals(result.get("statusCode"))){
//			returnFlag=Constants.returnObj.returnFlag_true;
//			//正常返回输出data包体信息（map）
//			HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
//			Set<String> keySet = data.keySet();
//			for(String key:keySet){
//				Object object = data.get(key);
//				System.out.println(key +" = "+object);
//			}
//		}else{
//			returnFlag=Constants.returnObj.returnFlag_flase;
//			//异常返回输出错误码和错误信息
//			System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
//		}
		return returnFlag;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String phone="13986211802,13554250365";
		//String tempid="91560";
		String token="22222";//验证码参数
		String params2="1";//时间参数
		//String accountSid="8a48b55150d142790150d57f786a1817";
		//String accountToken="5f10bca622fa44fb8c875b62c470317f";
		//String appId="8a216da85519f4540155246d20850782";
		
		String accountSid="8aaf0708559f32dd0155a061b0e70089";
		String accountToken="ca8d98f8170b4293be592df629a8ea07";
		String appId="8aaf070855ed9ef10155fd08aee90737";
		
		String tempid="236323";//短信模版ID
		
		String hwtempid = "102729";
		String sptempid = "102727";
		
		String[] msg = new String[]{token,params2};
		
		System.out.println(sendSMS(accountSid, accountToken, appId, phone, tempid, msg));
		
//		System.out.println(sendSMS(accountSid, accountToken, appId, phone, hwtempid, null));
	}

}
