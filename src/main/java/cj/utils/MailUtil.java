package cj.utils;

import cj.utils.mail.MailSenderInfo;
import cj.utils.mail.SimpleMailSender;

public class MailUtil {
	
	public static boolean sendMail(String userName,String password,String toAddress,String subject,String content)
	{
		try {
			// 这个类主要是设置邮件
			MailSenderInfo mailInfo = new MailSenderInfo();
			mailInfo.setMailServerHost("smtp.163.com");
			mailInfo.setMailServerPort("25");
			mailInfo.setValidate(true);
			mailInfo.setUserName(userName);
			mailInfo.setPassword(password);// 您的邮箱密码
			mailInfo.setFromAddress(userName);
			mailInfo.setToAddress(toAddress);
			mailInfo.setSubject(subject);
			mailInfo.setContent(content);
			// 这个类主要来发送邮件
			SimpleMailSender sms = new SimpleMailSender();
//			sms.sendTextMail(mailInfo);// 发送文体格式
			sms.sendHtmlMail(mailInfo);// 发送html格式
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		
		return true;
	}

	public static void main(String[] args) {
		sendMail("1987hanmin@163.com", "82770265hm", "cjrbjt@163.com", "测试发送邮件subject", "测试发送邮件content <img src=''/>");
	}
}
