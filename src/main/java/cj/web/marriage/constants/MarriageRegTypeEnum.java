package cj.web.marriage.constants;


public enum MarriageRegTypeEnum {
	
	tesebanzheng("特色颁证", 0),
	jiehunyy("结婚预约", 1),
	buzhengyy("补证预约", 2),
	lihunyy("离婚预约", 3),
	;
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private MarriageRegTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (MarriageRegTypeEnum c : MarriageRegTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
