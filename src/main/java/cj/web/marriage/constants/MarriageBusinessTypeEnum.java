package cj.web.marriage.constants;

/**
 * 预约类型
 * @author Administrator
 *
 */
public enum MarriageBusinessTypeEnum {
	//预约类型（1 婚纱照预约 2 婚庆预约 3 婚宴预订）
	HSZYY("婚纱照预约",1),
	HQYY("婚庆预约", 2),
	HYYD("婚宴预订", 3),
	SJYD("商家预订", 4),
	;
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private MarriageBusinessTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (MarriageBusinessTypeEnum c : MarriageBusinessTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
