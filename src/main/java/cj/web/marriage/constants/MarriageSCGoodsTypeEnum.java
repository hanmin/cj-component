package cj.web.marriage.constants;

/**
 * 商城商品类型
 * @author Administrator
 *
 */
public enum MarriageSCGoodsTypeEnum {
	XN("虚拟商品", 1),
	ST("实体商品", 0);
	
	
    // 成员变量
    private String name;
    private int value;
    // 构造方法
    private MarriageSCGoodsTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (MarriageSCGoodsTypeEnum c : MarriageSCGoodsTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}