package cj.web.marriage.constants;

public enum MarriageSCIsInvoiceEnum {
	none("无",-1),
	person("个人", 0),
	company("公司", 1)
	;
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private MarriageSCIsInvoiceEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (MarriageSCIsInvoiceEnum c : MarriageSCIsInvoiceEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
