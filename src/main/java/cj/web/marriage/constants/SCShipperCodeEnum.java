package cj.web.marriage.constants;

public enum SCShipperCodeEnum {
	DBL("德邦","DBL"),
	EMS("EMS", "EMS"),
	HHTT("天天快递", "HHTT"),
	HTKY("百世快递", "HTKY"),
	SF("顺丰快递", "SF"),
	ST("申通快递", "ST"),
	YD("韵达快递", "YD"),
	YTO("圆通速递", "YTO"),
	ZTO("中通速递", "ZTO"),
	;
    // 成员变量
    private String name;
    private String value;

    // 构造方法
    private SCShipperCodeEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(String value) {
        for (SCShipperCodeEnum c : SCShipperCodeEnum.values()) {
            if (c.getValue().equals(value)) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
