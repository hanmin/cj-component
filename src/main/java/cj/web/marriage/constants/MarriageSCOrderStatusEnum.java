package cj.web.marriage.constants;

public enum MarriageSCOrderStatusEnum {

	cancel("交易关闭", 0),  //关闭取消的订单
	prepay("等待买家付款", 1),  //待支付的订单
	paid("等待卖家发货",2),     //已支付,待发货
	posted("已发货",3),   //已发货
	finish("交易成功",4)    //收货，已完成
	;
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private MarriageSCOrderStatusEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (MarriageSCOrderStatusEnum c : MarriageSCOrderStatusEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
