package cj.web.marriage.constants;

/**
 * 婚宴商家信息
 * @author Administrator
 *
 */
public enum YYWeddingFeastTypeEnum {
	LX("森系",0),
	ZS("中式", 1),
	XS("西式", 2),
	HWCP("户外草坪", 3),
	YL("游轮", 4),
	;
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private YYWeddingFeastTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (YYWeddingFeastTypeEnum c : YYWeddingFeastTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
