package cj.web.marriage.constants;


public enum MarriageRegStatusEnum {

	yuyue("预约", 1),
	agree("通过", 2),
	refuse("不通过", 3),
	;
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private MarriageRegStatusEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (MarriageRegStatusEnum c : MarriageRegStatusEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
