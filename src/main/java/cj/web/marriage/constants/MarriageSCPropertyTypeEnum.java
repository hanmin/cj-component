package cj.web.marriage.constants;

/**
 * 婚典商城商品属性
 * @author Administrator
 *
 */
public enum MarriageSCPropertyTypeEnum {

	LITERA("文字", 1),
	IMAGE("图片", 2);
	
	// 成员变量
    private String name;
    private int value;

    // 构造方法
    private MarriageSCPropertyTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (MarriageSCPropertyTypeEnum c : MarriageSCPropertyTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
