package cj.web.marriage.constants;

public enum RegAddressEnum {
	
	JA("江岸区", 1,"江岸区二七横路金涛翰林苑5号楼一楼（区政务中心旁)","82414472、82739285"),
	JH("江汉区", 2,"江汉区发展大道234号C区3楼（江汉交通大队旁）","83952798"),
	QK("硚口区", 3,"硚口区工农路6号，硚口区老年公寓二楼","83792740"),
	HY("汉阳区", 4,"汉阳区墨水湖105号","84635473"),
	WC("武昌区", 5,"武昌区中山路312号凤凰大厦西楼1-2楼","88938919"),
	HS("洪山区", 6,"洪山区珞狮南路318号区政务中心3楼","87678568"),
	QS("青山区", 7,"青山区友谊大道112街南侧青山区民政局婚姻登记处3楼","68865250"),
	DXH("东西湖区", 8,"东西湖区东吴大道1045号 东西湖区民政局一楼","83081079"),
	HN("汉南区", 9,"汉南区薇湖西路400号汉南区民政局1楼","84758055"),
	CD("蔡甸区", 10,"蔡甸区蔡甸大街981号蔡甸区民政局1楼","69844765"),
	JX("江夏区", 11,"江夏区纸坊街文化路江夏区民政局1楼","81819015"),
	HP("黄陂区", 12,"黄陂区百秀街183号民政局大楼3楼","61004200"),
	XZ("新洲区", 13,"新洲区邾城街齐安大道326号社区服务大楼4楼","86925551"),
	WHKF("武汉开发区", 14,"武汉市经济开发区珠山湖大道振华路47号","84896561"),
	DHFJ("东湖风景区", 15,"武汉市东湖风景区中北路东路25号栋1楼","86773376"),
	DHKF("东湖高新区", 16,"武汉市高新大道777号（高新大道与光谷四路交叉处）","67880551");
	
    // 成员变量
    private String name;
    private int value;
    private String address;
    private String tel;
    // 构造方法
    private RegAddressEnum(String name, int value,
    		String address,String tel) {
        this.name = name;
        this.value = value;
        this.address = address;
        this.tel = tel;
    }

    // 普通方法
    public static String getName(int value) {
        for (RegAddressEnum c : RegAddressEnum.values()) {
            if (c.getValue() == value) {
                return c.name+" "+c.address+" "+c.tel;
            }
        }
        return null;
    }
    
    // 普通方法
    public static String getNameOnly(int value) {
        for (RegAddressEnum c : RegAddressEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
}
