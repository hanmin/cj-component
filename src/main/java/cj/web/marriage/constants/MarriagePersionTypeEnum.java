package cj.web.marriage.constants;


public enum MarriagePersionTypeEnum {
	
	ndjm("居民", 0),
	xyjr("军人", 1),
	;
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private MarriagePersionTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (MarriagePersionTypeEnum c : MarriagePersionTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
