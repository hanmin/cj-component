package cj.web.admin.constants;

public enum SourceEnum {

	CJRB("长江日报", 0,"/images/newmedia/qrCode/cjrbqrcode.jpg","/images/default/cjrb_default.png"), 
	WHWB("武汉晚报", 1,"/images/newmedia/qrCode/whwbqrcode.jpg","/images/default/whwb_defaut.png"),
	WHCB("武汉晨报", 2,"/images/newmedia/qrCode/whcbqrcode.png","/images/default/whcb_default.png"), 
	JJW("长江网", 3,"/images/newmedia/qrCode/cjwqrcode.png","/images/default.jpg"),
	JTZH("集团综合", 4,"","/images/default.jpg"),
	HW("汉网", 5,"/images/newmedia/qrCode/hwqrcode.png","/images/default.jpg"),
	HYW("好医网", 6,"/images/newmedia/qrCode/hyqrcode.png","/images/default.jpg"),
	CC("楚才", 7,"/images/newmedia/qrCode/ccqrcode.png","/images/default.jpg"),
	FXGS("发行公司", 8,"","/images/default/cjfx_default.png"),
	CJKD("长江快递", 9,"","/images/default/cjkd_default.png"),
	YWGS("印务公司", 10,"","/images/default/cjyw_default.png"),
	MJYJS("媒介研究所", 11,"","/images/default.jpg"),
	WHX("武汉宣传", 12,"/images/newmedia/qrCode/whxcqrcode.jpg","/images/default.jpg"),
	TZSB("投资时报", 13,"/images/newmedia/qrCode/tzsbqrcode.png","/images/default.jpg"),
	RWB("人物汇报", 14,"","/images/default.jpg"),
	XDJKB("现代健康报", 15,"","/images/default.jpg"),
	KSZNB("考试指南报", 16,"","/images/default.jpg"),
	WHSB("武汉商报", 17,"","/images/default.jpg"),
	XDSNB("现代少年报", 18,"","/images/default.jpg"),
	JZB("家长报", 19,"","/images/default.jpg");
    // 成员变量
    private String name;
    private int value;
    private String qrCode;
    private String defaultImg;
    // 构造方法
    private SourceEnum(String name, int value,String qrCode,String defaultImg) {
        this.name = name;
        this.value = value;
        this.qrCode = qrCode;
        this.defaultImg = defaultImg;
    }

    // 普通方法=>name
    public static String getName(int value) {
        for (SourceEnum c : SourceEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }
    
    // 普通方法=>qrCode
    public static String getQrCode(int value) {
        for (SourceEnum c : SourceEnum.values()) {
            if (c.getValue() == value) {
                return c.qrCode;
            }
        }
        return null;
    }
    
    // 普通方法=>defaultImg
    public static String getDefaultImg(int value) {
        for (SourceEnum c : SourceEnum.values()) {
            if (c.getValue() == value) {
                return c.defaultImg;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getDefaultImg() {
		return defaultImg;
	}

	public void setDefaultImg(String defaultImg) {
		this.defaultImg = defaultImg;
	}
}
