package cj.web.admin.constants;

public enum KDRecovePaperWeightEnum {
	GJ30("10公斤",0),
	GJ40("20公斤",1),
	GJ50("30公斤",2),
	GJ50S("30公斤以上",3);
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private KDRecovePaperWeightEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (KDRecovePaperWeightEnum c : KDRecovePaperWeightEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
