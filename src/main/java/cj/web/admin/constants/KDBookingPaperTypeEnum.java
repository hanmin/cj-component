package cj.web.admin.constants;

public enum KDBookingPaperTypeEnum {
	CJRB("长江日报",0),
	WHCB("武汉晨报",1),
	WHWB("武汉晚报",2);
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private KDBookingPaperTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (KDBookingPaperTypeEnum c : KDBookingPaperTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
