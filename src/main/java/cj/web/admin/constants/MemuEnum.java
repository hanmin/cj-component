package cj.web.admin.constants;

/**
 * 菜单枚举
 * @author Administrator
 *
 */
public enum MemuEnum {
	//集团官网
		//首页
	cjtt("集团官网_首页_长江头条", "jtgw_home_cjtt"),
	wjh("集团官网_首页_微聚合", "jtgw_home_wjh"),
	jtdt("集团官网_首页_集团动态", "jtgw_home_jtdt"),
	pphd("集团官网_首页_品牌活动", "jtgw_home_pphd"),
		//关于集团
	hwls("集团官网_关于集团_回望历史", "jtgw_gyjt_hwls"),
		//报刊媒体-长江日报
	cjrbzxdt("集团官网_报刊媒体_长江日报_最新动态", "jtgw_bkmt_cjrb_cjrbzxdt"),
	cjrbpphd("集团官网_报刊媒体_长江日报_品牌活动", "jtgw_bkmt_cjrb_cjrbpphd"),
	cjrbpplm("集团官网_报刊媒体_长江日报_品牌栏目", "jtgw_bkmt_cjrb_cjrbpplm"),
	cjrbppzk("集团官网_报刊媒体_长江日报_品牌周刊", "jtgw_bkmt_cjrb_cjrbppzk"),
		//新兴媒体-新媒体矩阵
	wxh("集团官网_新兴媒体_新媒体矩阵_微信汇", "jtgw_xxmt_xmtjz_wxh"),
	wbh("集团官网_新兴媒体_新媒体矩阵_微博汇", "jtgw_xxmt_xmtjz_wbh"),
	app("集团官网_新兴媒体_新媒体矩阵_App", "jtgw_xxmt_xmtjz_app"),
	wz("集团官网_新兴媒体_新媒体矩阵_网站", "jtgw_xxmt_xmtjz_wz"),
		//新兴媒体-众创空间
	zxcg("集团官网_新兴媒体_众创空间_最新成果", "jtgw_xxmt_zckj_zxcg"),
	cyzn("集团官网_新兴媒体_众创空间_创业指南", "jtgw_xxmt_zckj_cyzn"),
	qygc("集团官网_新兴媒体_众创空间_前沿观察", "jtgw_xxmt_zckj_qygc"),
		//集团产业-印务公司
	ywgsdt("集团官网_集团产业_印务公司_动态", "jtgw_jtcy_ywgs_ywgsdt"),
		//集团产业-快递公司
	kdgsgsxw("集团官网_集团产业_快递公司_公司新闻", "jtgw_jtcy_kdgs_kdgsgsxw"),
	kdgszxhd("集团官网_集团产业_快递公司_最新活动", "jtgw_jtcy_kdgs_kdgszxhd"),
	kdgshydt("集团官网_集团产业_快递公司_行业动态", "jtgw_jtcy_kdgs_kdgshydt"),
	kdgsnrbj("集团官网_集团产业_快递公司_内容编辑", "jtgw_jtcy_kdgs_kdgsnrbj"),
	kdgszxdb("集团官网_集团产业_快递公司_在线订报", "jtgw_jtcy_kdgs_kdgszxdb"),
	kdgsdbkqy("集团官网_集团产业_快递公司_订报卡启用", "jtgw_jtcy_kdgs_kdgsdbkqy"),
	kdgsjbhs("集团官网_集团产业_快递公司_废报回收", "jtgw_jtcy_kdgs_kdgsjbhs"),
	kdgsgztb("集团官网_集团产业_快递公司_改址转投", "jtgw_jtcy_kdgs_kdgsgztb"),
	kdgstsjy("集团官网_集团产业_快递公司_投诉建议", "jtgw_jtcy_kdgs_kdgstsjy"),
	kdgsdbksj("集团官网_集团产业_快递公司_订报卡数据", "jtgw_jtcy_kdgs_kdgsdbksj"),
	kdgsdc("集团官网_集团产业_快递公司_数据导出", "jtgw_jtcy_kdgs_kdgsdc"),
		//用户中心-
	xwbl("集团官网_用户中心_新闻报料", "jtgw_uc_xwbl"),
	fxwd("集团官网_用户中心_发行网点", "jtgw_uc_fxwd"),
	btcx("集团官网_用户中心_报亭查询", "jtgw_uc_btcx"),
		//用户中心-下载中心
	ggxz("集团官网_用户中心_下载中心_公告下载", "jtgw_uc_xzzx_ggxz"),
	zlxz("集团官网_用户中心_下载中心_资料下载", "jtgw_uc_xzzx_zlxz"),
	
	//用户管理
	yhgrxx("后台用户管理_个人信息", "yhgl_yhgrxx"),
	yhll("后台用户管理_用户列表", "yhgl_yhll"),
	cdgl("后台用户管理_菜单管理", "yhgl_cdgl"),
	jsgl("后台用户管理_角色管理", "yhgl_jsgl"),
	
	//配置管理
	pzgl("配置管理_配置", "pzgl_pz"),
	shrybz("配置管理_审核人员配置", "pzgl_shrybz"),
	//活动管理
	whg("活动管理_武汉观", "hdgl_whg"),
	hou("活动管理_猴红包", "hdgl_hou"),
	fakehou("活动管理_猴红包商品", "hdgl_fakehou"),
	scgoods("活动管理_商城_商品", "hdgl_sc_scgoods"),
	scorder("活动管理_商城_订单", "hdgl_sc_scorder"),
	scuser("活动管理_商城_用户", "hdgl_sc_scuser"),
	
	vote("活动管理_投票管理", "hdgl_vote"),
	voteItem("活动管理_投票项管理", "hdgl_voteItem"),
	voteResult("活动管理_投票结果管理", "hdgl_voteResult"),
	
	//婚典管理
	marriagetj("婚典管理_提交信息", "marriage_marriagetj"),
	marriageref("婚典管理_结婚预约", "marriage_marriageref"),
	ceremonyref("婚典管理_颁证仪式预约", "marriage_ceremonyref"),
	marriageexport("婚典管理_导出数据", "marriage_marriageexport"),
	marriagexinfo("婚典管理_视频秀", "marriage_marriagexinfo"),
	setmeal("婚典管理_套餐管理", "marriage_setmeal"),
	userapl("婚典管理_用户粉丝", "marriage_userapl"),
	marriageschw("婚典管理_上传号外", "marriage_marriageschw"),
	marriageqyqx("婚典管理_区域权限", "marriage_marriageqyqx"),
	marriagejfgc("婚典管理_家风工程", "marriage_marriagejfgc"),

	marriagescbuyer("婚典商城_买家管理", "SC_SCbuyer"),
	marriagescseller("婚典商城_商家管理", "SC_SCseller"),
	marriagesccategory("婚典商城_商城管理_商品类型管理", "SC_SCmall_SCcategory"),
	marriagescproperty("婚典商城_商家管理_商品属性管理", "SC_SCmall_SCproperty"),
	marriagescgoods("婚典商城_商品管理", "SC_SCgoods"),
	marriagescorder("婚典商城_订单管理", "SC_SCorder"),	
	
	xqggbkgghd("婚典相亲_公共板块_公共活动", "XQ_ggbk_gghd"),
	xqggbkgghddd("婚典相亲_公共板块_公共活动订单", "XQ_ggbk_gghddd"),
	xqggbkggyydd("婚典相亲_公共板块_公共预约订单", "XQ_ggbk_ggyydd"),
	xqsqbksqhd("婚典相亲_社区板块_社区活动", "XQ_sqbk_sqhd"),
	xqsqbksqhddd("婚典相亲_社区板块_社区活动订单", "XQ_sqbk_sqhddd"),
	xqsqbksqyydd("婚典相亲_社区板块_社区预约订单", "XQ_sqbk_sqyydd"),
	xqxqhygl("婚典相亲_相亲会员管理", "XQ_xqhygl"),
	hyzlsc("婚典相亲_会员资料删除", "XQ_hyzlsc"),
	xqsqgl("婚典相亲_社区管理", "XQ_sqgl"),
	xqsjdc("婚典相亲_数据导出", "XQ_sjdc"),
	xqqfdx("婚典管理_群发短信", "XQ_qfdx"),
	
	yYBooking("预约管理_预约信息", "yy_yYBooking"),
	yYWeddingCelebration("预约管理_婚庆商品信息", "yy_yYWeddingCelebration"),
	yYWeddingFeast("预约管理_婚宴商家信息", "yy_yYWeddingFeast"),
	yYWeddingPhoto("预约管理_婚纱照商家信息", "yy_yYWeddingPhoto"),
	yYBusinessRes("预约管理_商家预约信息", "yy_yYBusinessRes"),
	//长江金服
	cjjfNews("长江金服_新闻列表", "cjjf_xwlb"),
	;
	
    // 成员变量
    private String name;
    private String value;

    // 构造方法
    private MemuEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(String value) {
        for (MemuEnum c : MemuEnum.values()) {
            if (c.getValue().equals(value)) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
