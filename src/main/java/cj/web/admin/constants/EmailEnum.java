package cj.web.admin.constants;

public enum EmailEnum {
	CJRB("长江日报官方邮箱","cjrb@cjrb.com"),WHWB("武汉晚报官方邮箱","wuwb@wuwb.com"),WHCB("武汉晨报官方邮箱","wucb@wucb.com");
    // 成员变量
    private String name;
    private String value;

    // 构造方法
    private EmailEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(String value) {
        for (EmailEnum c : EmailEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	

}
