package cj.web.admin.constants;

public enum KDCardEnum {
	WSY("未使用",0),
	YSY("已使用",1),
	YGQ("已过期",2);
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private KDCardEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (KDCardEnum c : KDCardEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
