package cj.web.admin.constants;

public enum DownloadTypeEnum {
	PRODUCT("新媒体产品",0),NOTICE("公告",1),INFO("资料",2);
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private DownloadTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (DownloadTypeEnum c : DownloadTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
