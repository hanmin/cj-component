package cj.web.admin.constants;

public enum IndustryCategoryEnum {
	// 集团产业类别
	YWGS_GSXW("印务公司-动态", 0),
	YWGS_CPLB("印务公司-产品列表", 1),
	KDGS_GSXW("快递公司-公司新闻", 2),
	KDGS_ZXHD("快递公司-最新活动", 3),
	KDGS_HYDT("快递公司-行业动态", 4),
	KDGS_NR("快递公司-内容", 5),
	CJJF_XW("长江金服-新闻列表", 6);
	// 成员变量
	private String name;
	private int value;

	// 构造方法
	private IndustryCategoryEnum(String name, int value) {
		this.name = name;
		this.value = value;
	}

	// 普通方法
	public static String getName(int value) {
		for (IndustryCategoryEnum c : IndustryCategoryEnum.values()) {
			if (c.getValue() == value) {
				return c.name;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
