package cj.web.admin.constants;

public enum NewsCategoryEnum {

	KONG("无", 0), GFWX("官方微信", 1), GYWX("公益微信", 2), SYWX("商业微信", 3);
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private NewsCategoryEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (NewsCategoryEnum c : NewsCategoryEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
