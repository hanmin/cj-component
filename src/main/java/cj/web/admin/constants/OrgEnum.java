package cj.web.admin.constants;

public enum OrgEnum {
	ZZJG("组织机构", 0), 
	JTCY("集团产业", 1);
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private OrgEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (OrgEnum c : OrgEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
