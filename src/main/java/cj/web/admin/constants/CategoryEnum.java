package cj.web.admin.constants;

public enum CategoryEnum {
	//长江头条
	CJTT_CJTT("长江头条-长江头条", 0), 
	CJTT_WBRY("长江头条-微聚合", 1), 
	CJTT_WXTT("长江头条-集团动态", 2), 
	CJTT_PPHD("长江头条-品牌活动", 3), 
	//长江日报
	CJRB_ZXDT("长江日报-最新动态", 4),
	CJRB_PPHD("长江日报-品牌活动", 5),
	CJRB_PPLM("长江日报-品牌栏目", 6),
	CJRB_PPZK("长江日报-品牌周刊", 7);
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private CategoryEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (CategoryEnum c : CategoryEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
