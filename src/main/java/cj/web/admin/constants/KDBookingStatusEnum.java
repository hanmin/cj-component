package cj.web.admin.constants;

public enum KDBookingStatusEnum {
	DCL("待处理",0),
	YCL("已处理",1);
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private KDBookingStatusEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (KDBookingStatusEnum c : KDBookingStatusEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
