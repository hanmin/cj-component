package cj.web.admin.constants;

public enum KDInTypeEnum {

	WZ("网站",0),
	WX("微信",1);
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private KDInTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (KDInTypeEnum c : KDInTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}