package cj.web.admin.constants;

public enum MatrixTypeEnum {

	WZ("网站", 0),  WB("微博", 1), WX("微信", 2), APP("App", 3), SZB("数字报", 4);
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private MatrixTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (MatrixTypeEnum c : MatrixTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
