package cj.web.admin.constants;

public enum AccessTrackEnum {

	// 需要监听的方法头
	create("create", 0),
	update("update", 1),
	delete("delete", 2),
	add("add", 3),
	mod("mod", 4),
	click("clickrate",5);

	// 成员变量
	private String name;
	private int value;

	// 构造方法
	private AccessTrackEnum(String name, int value) {
		this.name = name;
		this.value = value;
	}

	// 普通方法
	public static String getName(int value) {
		for (AccessTrackEnum c : AccessTrackEnum.values()) {
			if (c.getValue() == value) {
				return c.name;
			}
		}
		return null;
	}
	
	/**
     * 方法名称是否存在
     * @param name
     * @return
     */
    public static Boolean reMethodExist(String name)
    {
    	for (AccessTrackEnum c : AccessTrackEnum.values()) {
    		
    		if(name.toLowerCase().indexOf(c.getName().toLowerCase())!=-1)
    		{
    			return true;
    		}
		}
		return false;
    }
    
	public static Boolean methodExist(String name, AccessTrackEnum c) {
		if (name.toLowerCase().indexOf(c.getName().toLowerCase()) != -1) {
			return true;
		}

		return false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
