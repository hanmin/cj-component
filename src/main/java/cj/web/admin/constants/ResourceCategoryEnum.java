package cj.web.admin.constants;

public enum ResourceCategoryEnum {
	
	DSJ("大事记",0),
	ZXCG("最新成果",1),
	CYZN("创业指南",2),
	QYGC("前沿观察",3),
	DSJSJZ("大事记时间轴",4);	
	
	private String name;
	private int value;
	
	private  ResourceCategoryEnum(String name,int value){
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public static String getName(int value){
		for(ResourceCategoryEnum c : ResourceCategoryEnum.values()){
			if(c.getValue() == value){
				return c.getName();
			}	
		}
		
		return null;
	}

}
