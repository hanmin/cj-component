package cj.web.activity.constants;

/**
 * 商城商品类型
 * @author Administrator
 *
 */
public enum SCGoodsTypeEnum {

	//XSZQ("新生专区", 0,"new"),
	LSZQ("学生专区", 1,"old");
	
    // 成员变量
    private String name;
    private int value;
    private String code;
    // 构造方法
    private SCGoodsTypeEnum(String name, int value,String code) {
        this.name = name;
        this.value = value;
        this.code = code;
    }

    // 普通方法
    public static String getName(int value) {
        for (SCGoodsTypeEnum c : SCGoodsTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }
    
    public static String getCode(int value) {
        for (SCGoodsTypeEnum c : SCGoodsTypeEnum.values()) {
            if (c.getValue() == value) {
                return c.code;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
