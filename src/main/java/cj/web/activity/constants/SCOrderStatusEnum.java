package cj.web.activity.constants;

public enum SCOrderStatusEnum {

	WXF("未消费", 0),
	YXF("已消费", 1);
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private SCOrderStatusEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (SCOrderStatusEnum c : SCOrderStatusEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
