package cj.web.activity.constants;

public enum SCGoodsStatusEnum {

	SPSJ("上架", 0),
	SPXJ("下架", 1);
	
    // 成员变量
    private String name;
    private int value;

    // 构造方法
    private SCGoodsStatusEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    // 普通方法
    public static String getName(int value) {
        for (SCGoodsStatusEnum c : SCGoodsStatusEnum.values()) {
            if (c.getValue() == value) {
                return c.name;
            }
        }
        return null;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
